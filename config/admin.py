from django.contrib import admin

from config.models import ParamsUrlConfig


class ParamsUrlConfigAdmin(admin.ModelAdmin):
    pass

admin.site.register(ParamsUrlConfig, ParamsUrlConfigAdmin)