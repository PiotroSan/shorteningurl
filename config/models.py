from django.db import models


class ParamsUrlConfig(models.Model):
    params_name = models.CharField(max_length=50)
    value = models.CharField(max_length=50)
