import json
import requests
import urllib

from shorteningurl import settings
from api.randomuser.tools import get_nested_dict_value


class BuilderAPIParams:

    def __init__(self, product):
        self._product = product
        self._result = None
        self._gender = None
        self._seeds = None
        self._url_params = {}

    # Custom builder pattern
    def set_result(self, value):
        self._result = value

    def set_gender(self, value):
        self._gender = value

    def set_seeds(self, value):
        self._seeds = value

    def _build_result(self):
        if not self._result:
            return
        self._url_params.update({'results': self._result})

    def _build_gender(self):
        if not self._gender:
            return
        self._url_params.update({'gender': self._gender})

    def _build_seeds(self):
        if not self._seeds:
            return
        self._url_params.update({'seed': self._seeds})

    def build_params(self):
        self._build_result()
        self._build_gender()
        self._build_seeds()
        self._product.params = self._url_params


class ProductAPI:

    def __init__(self, url=None):
        self.url = url or settings.RANDOM_USER_URL
        self.params = None

    def _get_user(self):
        response = requests.get(self.url, params=self.params)
        return response.content.decode(encoding='UTF-8', errors='strict')

    def user_data_list(self):
        users = json.loads(self._get_user())

        return [{
            'first_name': get_nested_dict_value(user, ['name', 'first']),
            'last_name': get_nested_dict_value(user, ['name', 'last']),
            'username': get_nested_dict_value(user, ['login', 'username']),
            'email': user.get('email'),
            'password1': get_nested_dict_value(user, ['login', 'password']),
            'password2': get_nested_dict_value(user, ['login', 'password']),
            'date_joined': get_nested_dict_value(user, ['registered', 'date']),
        } for user in users.get('results')]


class DirectorAPI:

    def __init__(self, builder):
        self._builder = builder

    def build(self):
        self._builder.build_params()
