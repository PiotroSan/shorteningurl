
def get_nested_dict_value(container, ordered_list):
    for value in ordered_list:
        try:
            container = container.get(value, '')
        finally:
            if not isinstance(container, dict):
                return container
