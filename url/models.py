from django.db import models
from django.contrib.auth.models import User

from url.shortening import ShorteningUrl


class Url(models.Model):
    desination = models.CharField(max_length=100, unique=True)
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='urls',
        null=True,
        blank=True,
    )

    @property
    def short_url(self):
        return ShorteningUrl.get_shortening_url(self.id)

    @property
    def full_short_url(self):
        return ShorteningUrl.get_full_shortening_url(self.id)