from shorteningurl import settings

from url.enum.shortening_enums import ALPHABET
from config.models import ParamsUrlConfig


class ShorteningUrl:

    def __init__(self, alphabet=None):
        self.alphabet = alphabet or ALPHABET
        self.alp_size = len(self.alphabet)

    def _encode(self, value):
        short = []
        while value > 0:
            index = value % self.alp_size
            short.append(ALPHABET[index])
            value //= self.alp_size

        try:
            max_length = ParamsUrlConfig.objects.get(
                params_name='LEN'
            ).values('value')
        except ParamsUrlConfig.DoesNotExist:
            return ''.join(reversed(short))
        else:
            return ''.join(reversed(short[:max_length]))

    def _debase(self, url):
        return [self.alphabet.index(element) for element in url]

    def _decode(self, url):
        return sum([
            value**index
            for index, value in enumerate(self._debase(url))
        ])

    @classmethod
    def get_shortening_url(cls, value, alphabet=None):
        shorter = cls(alphabet)
        return shorter._encode(value)

    @classmethod
    def get_full_shortening_url(cls, value, alphabet=None):
        return 'http://{}/{}/'.format(
            settings.HOSTNAME,
            cls.get_shortening_url(value, alphabet)
        )

    @classmethod
    def get_url_id(cls, url, alphabet=None):
        shorter = cls(alphabet)
        return shorter._decode(url)
