from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.forms import UserCreationForm

from api.randomuser.api import (
    BuilderAPIParams,
    ProductAPI,
    DirectorAPI,
)


class Command(BaseCommand):
    help = 'Add fake user from https://randomuser.me/'

    def add_arguments(self, parser):
        parser.add_argument(
            '--useramount',
            type=int,
        )

    def handle(self, *args, **options):
        user_amount = options.get('useramount')

        random_user_api = ProductAPI()
        params = BuilderAPIParams(random_user_api)
        params.set_result(user_amount)
        params.build_params()
        director = DirectorAPI(params)
        director.build()
        users = random_user_api.user_data_list()

        for user in users:
            form = UserCreationForm(user)
            if form.is_valid():
                form.save()
