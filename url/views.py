from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseNotFound, HttpResponseRedirect

from url.models import Url
from url.shortening import ShorteningUrl


class UrlListView(ListView):
    model = Url


class UrlCreateView(CreateView):
    model = Url
    fields = '__all__'

    def get_success_url(self):
        return reverse('url-list')


def redirector(request, url):
    template = 'url/real_url.html'
    true_url = url.replace('!', '')

    try:
        original_url = Url.objects.get(
            id=ShorteningUrl.get_url_id(true_url)
        )
    except Url.DoesNotExist:
        return HttpResponseNotFound('<h1>Url not found</h1>')
    else:
        if url.startswith('!'):
            return HttpResponseRedirect(original_url.desination)
        else:
            return render(
                request,
                template,
                context={
                    'url': '!{}'.format(original_url.short_url),
                    'destination': original_url.desination,
                    'user': original_url.user

            })
